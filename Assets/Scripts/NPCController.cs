using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera m_Camera;
    public void EnableCamera(bool enable)
    {
        m_Camera.gameObject.SetActive(enable);
    }
}
