using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using UnityEngine.InputSystem;
using System;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject m_playerInteractionUI;
    [SerializeField] GameObject m_dialougeBox;
    private TextWriter m_textWriter;
    [SerializeField] TextMeshProUGUI m_dialougeText;
    [SerializeField] TextMeshProUGUI m_avatarNameText;
    [SerializeField] TextMeshProUGUI m_option1Text;
    [SerializeField] TextMeshProUGUI m_option2Text;
    public Image m_avatorPic;
    [SerializeField] GameObject m_OptionsBox;
    [SerializeField] float m_timePerCharacter = 0.1f;
    [SerializeField] private List<Story> m_story;

    public List<Sprite> AvatarPictures;

    private AudioSource m_audioSource;
    private int m_commonResponseIndex;
    private int m_commonResponses;
    private bool m_isCommonResponseAvailable = false;
    private int m_currentStoryIndex;
    private bool m_isDialougeOpned;
    public bool IsDialougeOpned { get { return m_isDialougeOpned; } }

    public static Action TriggerNPC;

    private void Start()
    {
        m_textWriter = gameObject.AddComponent<TextWriter>();
        m_textWriter.TimePerCharacter = m_timePerCharacter;
        m_audioSource = gameObject.AddComponent<AudioSource>();
        m_audioSource.loop = true;
        m_audioSource.playOnAwake = false;

        m_textWriter.OnWritingCompleted += OnWritingCompleted;

        //m_dialougeBox.SetActive(false);
        m_playerInteractionUI.SetActive(false);
        m_OptionsBox.SetActive(false);
        m_dialougeBox.transform.localScale = Vector3.zero;
    }

    public void SetInteractMessageVisibility(bool visible)
    {
        m_playerInteractionUI.SetActive(visible);
    }

    public void OpenDialouge()
    {
        m_isDialougeOpned = true;
        m_dialougeBox.LeanScale(Vector3.one, 0.1f);
        m_avatarNameText.text = "Alex";
        m_avatorPic.sprite = AvatarPictures[1];

        m_OptionsBox.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        m_option1Text.text = m_story[m_currentStoryIndex].Option1;
        m_option2Text.text = m_story[m_currentStoryIndex].Option2;

        InputSystem.DisableDevice(Keyboard.current);

        //m_textWriter.AddWriter(m_dialougeText, "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, ", m_timePerCharacter);
        //m_audioSource.Play();
    }

    IEnumerator CloseDialougeWithDelay()
    {
        yield return new WaitForSeconds(2);

        m_isDialougeOpned = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        m_dialougeText.text = "";
        m_dialougeBox.LeanScale(Vector3.zero, 0.5f).setEaseInOutExpo();
        InputSystem.EnableDevice(Keyboard.current);
    }

    private void OnWritingCompleted()
    {
        m_audioSource.Stop();
        //StartCoroutine(CloseDialouge());

        if (m_isCommonResponseAvailable)
        {
            if (m_commonResponseIndex < m_story[m_currentStoryIndex].CommonResponses.Capacity)
            {
                StartCoroutine(WriteCommonResponseWithDelay(1.0f));
            }
            else
            {
                m_commonResponseIndex = 0;
                m_isCommonResponseAvailable = false;

                m_currentStoryIndex++;
                if (m_currentStoryIndex < m_story.Capacity)
                {
                    StartCoroutine(OpenNextDialougeWithDelay(1.0f));
                }
                else
                {
                    StartCoroutine(CloseDialougeWithDelay());
                }
            }
        }
    }

    public void OpenNextDialouge()
    {
        m_OptionsBox.SetActive(true);
        m_dialougeText.text = "";
        m_avatarNameText.text = "Alex";
        m_avatorPic.sprite = AvatarPictures[1];

        m_option1Text.text = m_story[m_currentStoryIndex].Option1;
        m_option2Text.text = m_story[m_currentStoryIndex].Option2;
    }

    private void WriteCommonResponse(string name, string response)
    {
        m_textWriter.AddWriter(m_dialougeText, response);
        m_avatarNameText.text = name;
        m_avatorPic.sprite = AvatarPictures[0];
    }


    public void Option1Selected()
    {
        m_OptionsBox.SetActive(false);
        m_dialougeText.gameObject.SetActive(true);
        m_textWriter.AddWriter(m_dialougeText, m_story[m_currentStoryIndex].Response1);
        m_isCommonResponseAvailable = m_story[m_currentStoryIndex].IsCommonResponsesAvailable;
    }

    public void Option2Selected()
    {
        m_OptionsBox.SetActive(false);
        m_dialougeText.gameObject.SetActive(true);
        m_textWriter.AddWriter(m_dialougeText, m_story[m_currentStoryIndex].Response2);
        m_isCommonResponseAvailable = m_story[m_currentStoryIndex].IsCommonResponsesAvailable;
    }

    IEnumerator WriteCommonResponseWithDelay(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        string commonResponse = m_story[m_currentStoryIndex].CommonResponses[m_commonResponseIndex];
        if (commonResponse.StartsWith('^'))
        {
            StartCoroutine(CloseDialougeWithDelay()); 
            TriggerNPC.Invoke();
            Debug.Log("Waiting");

        }
        else
        {
            string[] subs = commonResponse.Split('@');
            WriteCommonResponse(subs[0], subs[1]);
            m_commonResponseIndex++;
        }
    }
    IEnumerator OpenNextDialougeWithDelay(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        OpenNextDialouge();
    }
}
