using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class SenarioLogic : MonoBehaviour
{
    private int nextAct = 0;
    public NavMeshAgent NPC_Lisa;
    public NavMeshAgent NPC_Nico;
    public NavMeshAgent NPC_John;

    public TextMeshProUGUI text;



    public Transform pointBar;
    public Transform pointLisaSits;
    public Transform pointNicoSits;
    public Transform pointJohnSits;

    public CinemachineVirtualCamera virtualCameraPlayer;
    public CinemachineFreeLook freeCameraNPC;
    private Transform player;


    private void Awake()
    {
        player = GetComponent<Transform>();
        UIManager.TriggerNPC += ContinueActs;
    }


    private void PlaySenario()
    {
        switch (nextAct)
        {
            case 0:

                //Debug.Log("Nothing");
                text.text = "Act: 0";
                break;
            case 1:
                NPC_Lisa.destination = pointBar.position;
                //freeCameraNPC.gameObject.SetActive(true);
                //freeCameraNPC.LookAt = NPC_Lisa.transform;
                text.text = "Act: 1";
                break;
            case 2:
                NPC_Lisa.destination = pointLisaSits.position;
                //freeCameraNPC.LookAt = NPC_Lisa.transform;
                text.text = "Act: 2";
                break;
            case 3:
                text.text = "Act: 3 Sam and Alex talking";
                NPC_Nico.destination = pointBar.position;
                //freeCameraNPC.gameObject.SetActive(false);
                break;
            case 4:
                text.text = "Act: 4";
                NPC_Nico.destination = pointNicoSits.position;
                break;
            case 5:
                text.text = "Act: 5";
                NPC_John.destination = pointBar.position;
                break;
            case 6:
                text.text = "Act: 6 Sam and Alex talking";
                NPC_John.destination = pointJohnSits.position;
                break;
            case 7:
                text.text = "Act: 7";
                
                break;
            case 8:
                text.text = "Act: 8";
                SceneManager.LoadScene("GameWonScene");
                break;
            case 9:
                text.text = "Act: 9 Sam and Alex talking";
                break;
            case 10:
                text.text = "Act: 10";
                break;

        }

    }



    private void NextAct()
    {
        nextAct++;
    }

    private void ForceAct()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            nextAct++;
        }
    }


    private void ContinueActs()
    {
        nextAct++;
        if (nextAct == 1)
        {
            StartCoroutine(ContinueSenario(5f));
        }
    }

    IEnumerator ContinueSenario(float seconds)
    {
        
        yield return new WaitForSeconds(seconds);
        NextAct();
        yield return new WaitForSeconds(seconds);
        NextAct();
        yield return new WaitForSeconds(seconds);
        NextAct();
        yield return new WaitForSeconds(seconds);
        NextAct();
        yield return new WaitForSeconds(seconds);
        NextAct();
        yield return new WaitForSeconds(seconds);
        NextAct();
        yield return new WaitForSeconds(seconds);
        NextAct();
        yield return new WaitForSeconds(seconds);
        NextAct();

    }

    // Update is called once per frame
    void Update()
    {
        ForceAct();
        PlaySenario();
        //ContinueActs();
    }
}
/*
 
 if (Input.GetKeyDown(KeyCode.N))
        {
           
            
        }
 
 */