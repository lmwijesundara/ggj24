using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Story", menuName = "ScriptableObjects/Story")]
public class Story : ScriptableObject
{
    public string Option1;
    public string Option2;
    public string Response1;
    public string Response2;
    public bool IsCommonResponsesAvailable;
    public List<string> CommonResponses;

    Dictionary<string, int> playerScores;
}
