using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    public GameObject inGameUI;
    public GameObject pauseUI;
    private bool on = false;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (on)
            {
                inGameUI.SetActive(true);
                pauseUI.SetActive(false);
                Time.timeScale = 1;
                on = false;
            }
            else
            {
                inGameUI.SetActive(false);
                pauseUI.SetActive(true);
                Time.timeScale = 0;
                on = true;
            }

        }
    }
}
