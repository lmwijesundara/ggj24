using TMPro;
using UnityEngine;

public class NPCNames : MonoBehaviour
{
    private Transform NPC;
    [SerializeField] public string characterName;
    // Start is called before the first frame update
    void Start()
    {
        NPC = GetComponent<Transform>();
        NPC.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = characterName;
    }
}
