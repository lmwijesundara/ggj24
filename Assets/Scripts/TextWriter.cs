using TMPro;
using UnityEngine;
using System;
using System.Collections.Generic;


public class TextWriter : MonoBehaviour
{
    private TextMeshProUGUI m_uiText;
    private string m_textToWrite;
    private int m_characterIndex;
    private float m_timePerCharacter;
    public float TimePerCharacter { get { return m_timePerCharacter; } set { m_timePerCharacter = value; } }
    private float timer;
    public bool m_isCommonResponsesAvailable;
    public List<string> m_commonResponses;

    public event Action OnWritingCompleted;

    public void AddWriter(TextMeshProUGUI uiText, string textToWrite)
    {
        m_uiText = uiText;
        m_textToWrite = textToWrite;
        m_characterIndex = 0;
    }

    private void Update()
    {
        if (m_uiText != null) 
        { 
            timer -= Time.deltaTime;

            while (timer <= 0) 
            {
                timer += m_timePerCharacter;
                m_characterIndex++;
                m_uiText.text = m_textToWrite.Substring(0, m_characterIndex);

                if (m_characterIndex >= m_textToWrite.Length)
                {
                    if (m_isCommonResponsesAvailable)
                    {
                        m_uiText.text = "";
                        m_uiText.text = m_commonResponses[0];
                        m_characterIndex = 0;
                    }
                    else
                    {
                        m_uiText = null;
                        OnWritingCompleted?.Invoke();
                        return;
                    }
                }
            }
        }
    }
}
