using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCLogic : MonoBehaviour
{
    private NavMeshAgent NPC;
    public Transform point;

    bool on = false;

    void Start()
    {
        NPC = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            on = true;
        }

        if (on)
        {
            NPC.destination = point.position;

        }

    }
}
